# config valid only for current version of Capistrano
lock '3.4.1'

set :application, 'indicadores'
set :repo_url, 'git@bitbucket.org:jorgeosorio/indicadores.git'


# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default deploy_to directory is /var/www/my_app_name
set :deploy_to, '/home/tecnosystems/indicadores'

# Default value for :scm is :git
set :scm, :git

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
set :log_level, :debug

# Default value for :pty is false
set :pty, true
set :use_sudo, true

# Default value for :linked_files is []
set :linked_files, fetch(:linked_files, []).push('config/database.yml', 'config/secrets.yml')

# Default value for linked_dirs is []
set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system')

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

before 'deploy:check:linked_files', 'config:push'

# Default value for keep_releases is 5
set :keep_releases, 5

after 'deploy:published', 'restart' do
  invoke 'delayed_job:restart'
end

namespace :deploy do

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
    end
  end

  namespace :db do

    desc "Reload the database with seed data"
    task :seed do
      on primary :db do
        within release_path do
          with rails_env: fetch(:stage) do
            execute :rake, 'db:seed'
          end
        end
      end
    end

    task :create do
      on primary :db do
        within release_path do
          with rails_env: fetch(:stage) do
            execute :rake, 'db:create'
          end
        end
      end
    end

    task :migrate do
      on primary :db do
        within release_path do
          with rails_env: fetch(:stage) do
            execute :rake, 'db:migrate'
          end
        end
      end
    end

    task :reset do
      on primary :db do
        within release_path do
          with rails_env: fetch(:stage) do
            execute :rake, 'db:reset'
          end
        end
      end
    end

    task :drop do
      on primary :db do
        within release_path do
          with rails_env: fetch(:stage) do
            execute :rake, 'db:drop'
          end
        end
      end
    end

    task :setup do
      on primary :db do
        within release_path do
          with rails_env: fetch(:stage) do
            execute :rake, 'db:setup'
          end
        end
      end
    end

  end

end
