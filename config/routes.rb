Rails.application.routes.draw do

  root 'home#index'

  resources :home, only: [] do
    collection do
      get :search
      post :find
      post :export
      post :exclude
    end
  end

  resources :data, only: [:index] do
    collection do
      post :join
    end
  end
  resources :batches, only: [:index, :show]
  resources :payments, only: [] do
    collection do
      get :upload
      post :transfer
      get "/:batch_id/columns" => "payments#column", as: "columns"
      post "/:batch_id/process" => "payments#process_batch", as: "process"
    end
  end
  resources :infringements, only: [:show] do
    collection do
      get :upload
      post :transfer
      get "/:batch_id/columns" => "infringements#column", as: "columns"
      post "/:batch_id/process" => "infringements#process_batch", as: "process"
    end
  end

end
