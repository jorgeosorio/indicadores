class CreateInfringements < ActiveRecord::Migration
  def change
    create_table :infringements do |t|
      t.string :code
      t.string :infringement_type
      t.date :date
      t.boolean :active
      t.timestamps null: false
    end
  end
end
