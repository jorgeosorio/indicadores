class CreateBatches < ActiveRecord::Migration
  def change
    create_table :batches do |t|
      t.integer :failed
      t.integer :imported
      t.integer :total
      t.string :import_type
      t.string :import_running
      t.string :status
      t.string :file_name
      t.timestamps null: false
    end
  end
end
