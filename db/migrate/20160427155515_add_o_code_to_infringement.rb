class AddOCodeToInfringement < ActiveRecord::Migration
  def change
    add_column :infringements, :o_infringement_type, :string
  end
end
