class AddIndexToInfringement < ActiveRecord::Migration
  def change
    add_index :infringements, :code, unique: true
  end
end
