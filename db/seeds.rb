# # This file should contain all the record creation needed to seed the database with its default values.
# # The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
# #
# # Examples:
# #
# #   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
# #   Mayor.create(name: 'Emanuel', city: cities.first)
#
# def read_data_and_create column
#   i = false
#   column.each do |v|
#     if i == false
#       i = true
#       next
#     end
#     unless (i = Infringement.new code: v.value).save
#       puts "#{i.code} - #{i.errors.full_messages}"
#     end
#   end
# end
#
# # Read xlsx file
# xlsx_path = "./resources/Pagos Ita del 08_07_14 al 15_02_2016.xlsx"
# w = Oxcelix::Workbook.new(xlsx_path)
# read_data_and_create w.sheets[0].column(14)
# read_data_and_create w.sheets[1].column(3)
