class PaymentsController < ImporterController

  before_action :set_batch, only: [:column, :process_batch]
  before_action :check_for_csv, only: [:transfer]
  before_action :file_name, only: [:transfer]

  # GET
  def upload
  end

  # POST
  # (Subir archivos) direcciona a listar columnas
  def transfer
    # Crear un batch process, de tipo payment
    @batch = Batch.create(import_type: "payment", import_running: false, status: "pending", file_name: @file_name)
    # Recibir el archivo enviado y guardar en las rutas
    process_transfer "#{@batch.id}_c", "#{@batch.id}_d", columns_payments_path(@batch)
  end

  # GET
  # Leer la columna del archivo a importar
  def column
    @columns = read_column "#{@batch.id}_c"
  end

  # POST
  # Realizar el proceso de importación de pagos
  # TODO: Realizae este proceso en un delayed job
  def process_batch
    PaymentsController.delay.process_batch_delayed(@batch, params)
    return redirect_to @batch, notice: "La importación de pagos se está procesando."
  end

  private

  def self.process_batch_delayed batch, params
    # Parámetros requerido (Columna)
    _p = params.require(:csv).permit(:column); _column = _p[:column].to_i
    # Recorrer datos
    inserts = []
    traverse_data(batch) do |row|
      # payment = Payment.new(code: row[_column])
      # if payment.save
      #   @imported = @imported + 1
      # else
      #   @failed = @failed + 1
      # end
      code = "";
      code = row[_column].strip unless row[_column].nil?
      if code != ""
        inserts.push "('#{code}', '#{Time.now}', '#{Time.now}')"
        @imported = @imported + 1
      else
        @failed = @failed + 1
      end
      @total = @total + 1
      batch.update_attributes(total: @total, failed: @failed, imported: @imported)
    end
    sql = "INSERT INTO payments (code, created_at, updated_at) VALUES #{inserts.join(",")}"
    ActiveRecord::Base.transaction do
      ActiveRecord::Base.connection.execute sql
    end
  end

  def set_batch
    @batch = Batch.find(params[:batch_id])
  end

end
