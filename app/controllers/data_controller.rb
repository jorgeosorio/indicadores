class DataController < ApplicationController

  def index
    @payments = Payment.count()
  end

  def join
    DataController.delay.join_delayed
    return redirect_to root_path, notice: "El proceso de cruze de pagos y estados de comparendos está en ejecución."
  end

  private

  def self.join_delayed
    # Actualizar los comparendos
    @infringements = Infringement.where(code: Payment.select('DISTINCT code').pluck(:code)).update_all(active: true)
    @infringements_data = [
      {
        label: "Marcados",
        value: @infringements
      },
      {
        label: "Inactivos",
        value: Infringement.where(active: [nil, false]).count
      },
      {
        label: "Activos",
        value: Infringement.where(active: true).count
      }
    ]
    # Borrar elementos
    Payment.delete_all
  end

end
