class BatchesController < ApplicationController

  before_action :set_batch, only: [:show]

  def index
    @batches = Batch.order("created_at desc")
  end

  def show
    if @batch.import_type == "payment"
      @batch_data = [
        { label: "Importados", value: @batch.imported }
      ]
    else
      @batch_data = [
        { label: "Importados", value: @batch.imported },
        { label: "Repetidos", value: @batch.failed }
      ]
    end
  end

  private

  def set_batch
    @batch = Batch.find(params[:id])
  end

end
