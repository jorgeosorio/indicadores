class ImporterController < ApplicationController

  require 'csv'

  # Recibe la ruta de las columnas @columns y los datos @data
  # al finalizar direcciona a @redirect_to_path
  def process_transfer columns, data, redirect_to_path
    _columns = ""
    # Leer primera línea del archivo csv
    File.open(@csv.tempfile.path) { |f| _columns = f.readline }
    # Guardar un archivo con las columnas del csv
    File.open("./files/#{columns}.csv", 'w') { |file| file.write(_columns) }
    # Copiar del temporal a la carpeta de archivos
    FileUtils.cp(@csv.tempfile.path, "./files/#{data}.csv")
    # Enviar a elegir las columnas
    return redirect_to redirect_to_path
  end

  # Permite elegir el nombre de la columna a leer del archivo temporal de datos
  def read_column columns
    _columns = []
    # Leer archivo de columnas
    CSV.foreach("./files/#{columns}.csv", col_sep: ",") do |row|
      # Leer la primera columna o primera fila
      _i = -1
      row.each do |i|
        _columns << [i, _i=_i+1]
      end
      break
    end
    _columns
  end

  def self.traverse_data batch
    # Para ignorar la primera columna
    i = false
    # Variables estadísticas
    @total = 0; @failed = 0; @imported = 0;
    # Actualizar el estado del procedimiento
    batch.update_attributes(status: "importing", import_running: true)
    # Atomic inserts
    # ActiveRecord::Base.transaction do
    CSV.foreach("./files/#{batch.id}_d.csv", col_sep: ",") do |row|
      if i == false
        i = true
        next
      end
      yield row
    end
    # end
    # Actualizar estado
    batch.update_attributes(status: "imported", import_running: false, total: @total, failed: @failed, imported: @imported)
  end

  # Obtiene el nombre del archivo montado al servidor
  def file_name
    # Parámetro requerido
    _p = params.require(:csv).permit(:file)
    # Validar que exista un archivo
    unless (csv = _p[:file]).nil?
      return @file_name = csv.original_filename
    end
    return @file_name = ""
  end

  # ~-~~--~-~~--~-~~--~-~~--~-~~--~-~~--
  # Callbacks (before_action)
  # ~-~~--~-~~--~-~~--~-~~--~-~~--~-~~--

  # Validad que el archivo montado sea formato CSV
  def check_for_csv
    # Parámetro requerido
    _p = params.require(:csv).permit(:file)
    # Validar que exista un archivo
    unless (@csv = _p[:file]).nil?
      # Sólo permitir extension de archivo csv
      if File.extname(@csv.tempfile.path) == ".csv"
        return true
      else
        @error = "La extensión del archivo, no corresponde a un formato de CSV."
      end
    else
      @error = "Debe subir un archivo de formato csv."
    end
    return render :upload
  end

end
