class HomeController < ApplicationController

  require 'csv'

  def index

    total_infringements = Infringement.select("count(*) as total, infringement_type").where(active: [false, nil]).group("infringement_type")

    active_infringements = Infringement.select("count(*) as total, infringement_type").where(active: true).group("infringement_type")

    @total_infringements = to_json total_infringements
    @active_infringements = to_json active_infringements

  end

  def search
  end

  def exclude
    items = []
    CSV.foreach("./resources/ITA_COMPARENDOS_CRUCE.csv", col_sep: ",") do |row|
      items << row[0]
    end
    @ghosts = Infringement.where.not(code: items)
  end

  def export
    active = {}
    if params[:call] == "pending"
      active = {active: [false, nil]}
    elsif params[:call] == "active"
      active = {active: [true]}
    end

    @automaticos = Infringement.where(infringement_type: "AUTOMÁTICOS").where(active)
    @exonerados = Infringement.where(infringement_type: "EXONERADOS").where(active)
    @sancionados = Infringement.where(infringement_type: "SANCIONADOS EN AUDIENCIA").where(active)
    @rp = Infringement.where(infringement_type: "RP").where(active)
    @rc = Infringement.where(infringement_type: "RC").where(active)
    @caducidad = Infringement.where(infringement_type: "CADUCIDAD").where(active)
    @avisos = Infringement.where(infringement_type: "AVISOS").where(active)
    @revocatoria = Infringement.where(infringement_type: "REVOCATORIA").where(active)
    @resolucion = Infringement.where(infringement_type: "NÚMERO DE RESOLUCIÓN").where(active)
    respond_to do |format|
      format.xlsx
    end
  end

  def find
    respond_to do |format|
      unless (@infringement = Infringement.where(params.require(:search).permit(:code)).first).nil?
        format.html { redirect_to infringement_path(@infringement) }
      else
        format.html { redirect_to search_home_index_path, notice: "No se encontró comparendo." }
      end
    end
  end

  private

  def to_json infringements
    infringements_json = []
    infringements.each do |i|
      infringements_json << {label: i.infringement_type, value: i.total}
    end
    return infringements_json.to_json
  end

end
