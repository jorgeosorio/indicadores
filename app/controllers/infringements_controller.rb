class InfringementsController < ImporterController

  before_action :set_batch, only: [:column, :process_batch]
  before_action :check_for_csv, only: [:transfer]
  before_action :file_name, only: [:transfer]

  def show
    @infringement = Infringement.find(params[:id])
  end

  # GET
  def upload
  end

  # POST
  # (Subir archivos) direcciona a listar columnas
  def transfer
    # Crear un batch process, de tipo payment
    @batch = Batch.create(import_type: "infringement", import_running: false, status: "pending", file_name: @file_name)
    # Recibir el archivo enviado y guardar en las rutas
    process_transfer "#{@batch.id}_c", "#{@batch.id}_d", columns_infringements_path(@batch)
  end

  # GET
  # Leer la columna del archivo a importar
  def column
    @columns = read_column "#{@batch.id}_c"
  end

  # POST
  # Realizar el proceso de importación de pagos
  # TODO: Realizae este proceso en un delayed job
  def process_batch
    InfringementsController.delay.process_batch_delayed(@batch, params)
    return redirect_to @batch, notice: "La importación de comparendos se está procesando."
  end

  private

  def self.process_batch_delayed batch, params
    # Parámetros requerido (Columna)
    _p = params.require(:csv).permit(:infringement_type, :infringement_number, :infringement_date)
    infringement_type = _p[:infringement_type]
    infringement_number = _p[:infringement_number].to_i
    infringement_date = _p[:infringement_date].to_i
    # Recorrer datos
    traverse_data(batch) do |row|
      date = ""; code = "";
      code = row[infringement_number].strip unless row[infringement_number].nil?
      date = row[infringement_date].strip unless row[infringement_date].nil?
      if code != "" && date != ""
        parsed_date = Chronic.parse(date)
        unless parsed_date.nil?
          infringement = Infringement.new(code: code, infringement_type: infringement_type, date: parsed_date)
          if infringement.save
            @imported = @imported + 1
          else
            if parsed_date >= infringement.date
              infringement.update_attributes date: row[infringement_date], infringement_type: infringement_type
            end
            @failed = @failed + 1
          end
        else
          @failed = @failed + 1
        end
      else
        @failed = @failed + 1
      end
      @total = @total + 1
      batch.update_attributes(total: @total, failed: @failed, imported: @imported)
    end
  end

  def set_batch
    @batch = Batch.find(params[:batch_id])
  end

end
