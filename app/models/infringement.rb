class Infringement < ActiveRecord::Base

  before_create :set_o_infringement_type

  validates :code, uniqueness: true

  def set_o_infringement_type
    self.o_infringement_type = self.infringement_type
  end

end
