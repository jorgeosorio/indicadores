class Batch < ActiveRecord::Base

  scope :pendings, -> { where(status: :pending) }

  def self.pending
    Batch.pendings.first
  end

  def html_status
    return "Importado" if status == "imported"
    return "Esperando importación" if status == "pending"
    return "Importando..." if status == "importing"
  end

  def html_type
    return "Pagos" if import_type == "payment"
    return "Comparendos" if import_type == "infringement"
  end

end
